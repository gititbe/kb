<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Article;

/**
 * ArticleSearch represents the model behind the search form of `app\models\Article`.
 */
class ArticleSearch extends Article
{
    /**
     * @inheritdoc
     */

    public $tag; // הגדיר תכונה שנקראת טאגס

     //תכונה שלא מופיעה פה, אני לא מעדכן אותה
    public function rules()
    {
        return [
            [['id', 'author_id', 'editor_id', 'category_id', 'created_by', 'updated_by'], 'integer'],
            [['title', 'descriptin', 'body', 'created_at', 'updated_at', 'tag'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) // מקבלת את כל הפרמטרים שהיו בשורת ה url
                                    // והפכו לפרמטרים בכתיבה מובנת
    {
        $query = Article::find(); // סלקט כוכבית מארטיקל. מביא את כל המאמרים כי אין וור

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);  // כעת מכיר את כל הפרמטרים

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([ // פונקציה שמוסיפה תנאים לקווארי קיים
            'id' => $this->id, // מדלג על התנאי אם הערך null
            'author_id' => $this->author_id,
            'editor_id' => $this->editor_id,
            'category_id' => $this->category_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);
            // פרמטרים מסוג טקסט, במקום שיווין מתייחס עם לייק
        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'descriptin', $this->descriptin])
            ->andFilterWhere(['like', 'body', $this->body]);

        //add tags condition
        if (!empty($this->tags))
        {
            // רוצים לראות את כל המאמרים שמתוייגים באותו תג

            $condition = Tag::find()->select('id')->where(['IN' , 'name' , $this->tag]); // יוציא את האיי די של התגית, במקרה שלנו php
            $query->joinWith('tags'); // מקבל ג'וין של הארטיקל . מופיע כתכונה כי זה פונקציית גטר
            $query->andWhere(['IN' , 'tag_id' , $condition]);
       
        }
        return $dataProvider;
    }
}