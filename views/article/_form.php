<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;// אקטיבפורם מגדירים שדות שונים מסוגים שונים
use yii\helpers\ArrayHelper;
use app\models\Category;//על מנת להתשמש בדרום
use dosamigos\selectize\SelectizeTextInput;
use dosamigos\selectize\SelectizeDropDownList;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'descriptin')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>


<?= $form->field($model, 'tagNames')->widget(SelectizeTextInput::className(), [
// calls an action that returns a JSON object with matched
// tags
'loadUrl' => ['tag/list'],
'options' => ['class' => 'form-control'],
'clientOptions' => [
    'plugins' => ['remove_button'],
    'valueField' => 'name',
    'labelField' => 'name',
    'searchField' => ['name'],//ברגע שאכתוב מילה אחת זה יביא את התוצאות
    'create' => true,
],
])->hint('Use commas to separate tags') ?>


<?= $form->field($model, 'author_id')->textInput() ?>

<?= $form->field($model, 'editor_id')->textInput() ?>

<?= $form->field($model, 'category_id')->dropDownList(
    ArrayHelper::map(Category::find()->asArray()->all(), 'id', 'name')//מציג את כל הרשומות מטבלת category ורק את השדות איי די וניימ
) ?>

<?= $form->field($model, 'created_at')->textInput() ?>

<?= $form->field($model, 'updated_at')->textInput() ?>

<?= $form->field($model, 'created_by')->textInput() ?>

<?= $form->field($model, 'updated_by')->textInput() ?>

<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>

</div>
